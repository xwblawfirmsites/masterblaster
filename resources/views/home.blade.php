@extends('layouts.admin')

@section('content')
<div class="row no-gutters">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{$title}}</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                You are logged in!
            </div>
        </div>
    </div>
</div>
@endsection
