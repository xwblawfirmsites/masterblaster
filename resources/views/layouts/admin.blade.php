<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Master Blaster') }}</title>

    
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('plugins/DataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>
<body class="admin">
    <div id="app">
        <section>
            <aside>
                <div class="logo"></div>
                <div class="logged-user border-top border-bottom">
                    <p>Logged in as <strong>{{ Auth::user()->name }}</strong></p>
                </div>
                <nav>
                    <ul class="nav flex-column">
                      <li class="nav-item">
                        <a class="nav-link active" href="/"> <i class="fa fa-home"></i> Home </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{url('presales')}}"> <i class="fas fa-tags"></i> Pre Sale </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{url('server-account')}}"> <i class="fas fa-tags"></i> Server Account </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{url('people')}}"> <i class="fas fa-users"></i> People </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#"> <i class="fas fa-wrench"></i> Building </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="#"> <i class="fas fa-tag"></i> Post Sale  </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#"> <i class="fas fa-laptop"></i> Tasks </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#"> <i class="fas fa-comments"></i> Helpdesk </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#"> <i class="fas fa-user"></i> Admin </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#"> <i class="fas fa-tools"></i> Tools </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="" 
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                      </li>

                      
                    </ul>
                </nav>
            </aside>
            
                <header>

                    <nav class="nav-header navbar navbar-expand-md navbar-dark bg-primary shadow-sm">
                        <div class="header-title">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <h1>{{ config('app.name', 'Master Blaster') }}</h1>
                            </a>
                        </div>
                        <div class="navbar w-100">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <!-- Left Side Of Navbar -->
                                <ul class="navbar-nav mr-auto">

                                </ul>

                                <!-- Right Side Of Navbar -->
                                <ul class="navbar-nav ml-auto">
                                    <!-- Authentication Links -->
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                        @if (Route::has('register'))
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>
                                                <a class="dropdown-item" href="{{route('settings')}}">Settings</a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                    @endguest
                                </ul>
                            </div>
                        </div>
                        
                    </nav>
                    
                    @yield('navbar')
                </header>
                <article>
                    @yield('content')
                </article>
                <footer>
                    
                </footer>
        </section>

    </div>
    <!-- Scripts -->
    @yield('javascript')
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
    <script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>

    <script src="{{ asset('js/scripts.js') }}" defer></script>

    <script type="text/javascript">
        /*(function($) {
            // plugin code here, can use $ because it refers to our argument,
            // not the global symbol that may not be there
            bootbox.alert({
                size: "small",
                title: "Your Title",
                message: "Your message here…",
                callback: function(){

                }
            });
        })(jQuery, document, window);*/
    </script>

</body>
</html>
