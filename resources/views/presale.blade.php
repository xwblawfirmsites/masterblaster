@extends('layouts.admin')

@section('navbar')
<nav class="navbar navbar-dark bg-primary border border-dark">
    <ul class="nav">
        <li class="nav-item active"><a class="nav-link" href="#">My Pre Sales</a></li>
        <li class="nav-item"><a class="nav-link" href="#">Team Pre Sales</a></li>
    </ul>
    <ul class="nav">
        <li class="nav-item active"><a class="nav-link" href="#">Filter & Sort</a></li>
        <li class="nav-item"><a class="nav-link add-presale" href="#">Add Pre Sales</a></li>
    </ul>
</nav>
@endsection

@section('content')
<div class="row no-gutters">
    <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Presale run status</h5>
            <div class="row">
                <div class="col-md-2">
                    <i class="fa fa-spinner fa-3x fa-fw" aria-hidden="true"></i>

                </div>
                <div class="col-md-10">
                    <p>Emails: <span class="emails-count badge badge-info">{{$emailCount}}</span></p>
                    <p>Sent: <span class="email-sent badge badge-success">0</span></p>
                    <p>Failed: <span class="email-failed badge badge-danger">0</span></p>
                </div>
            </div>
            
          </div>
        </div>

        <div class="card">
            <div class="card-header">{{$title}}</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <table id="tbl-presale" class="table">
                    <thead>
                        <tr>
                            <th>PRE SALE</th>
                            <th>IN PROGRESS</th>
                            <th>DUE</th>
                            <th>STATS</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <a href="" class="btn btn-info xwb-test-email">Test Email</a>
    </div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
    var xwbVar = [];
    xwbVar['emailCount'] = '<?php echo $emailCount; ?>';
    xwbVar['sendPresaleURL'] = '<?php echo '/send-presale'; ?>';
</script>
@endsection