@extends('layouts.admin')

@section('navbar')
<nav class="navbar navbar-dark bg-primary border border-dark">
    
    <ul class="nav">
        <li class="nav-item"><a class="nav-link add-person" href="#">Add Person</a></li>
    </ul>
</nav>
@endsection

@section('content')
<div class="row no-gutters">
    <div class="col-md-12">


        <div class="card">
            <div class="card-header">{{$title}}</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <table id="tbl-people" class="table">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
    var xwbVar = [];
    
</script>
@endsection