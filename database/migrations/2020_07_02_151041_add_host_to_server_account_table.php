<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHostToServerAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server_accounts', function (Blueprint $table) {
            $table->string('host', 150)->nullable()->after('server_id');
            $table->enum('mail_driver', ['smtp', 'sendmail', 'mailgun', 'ses', 'postmark', 'localhost','other'])->default('sendmail')->after('host');
            $table->string('mail_from', 150)->nullable()->after('mail_driver');
            $table->string('from_name', 150)->nullable()->after('mail_from');
            $table->boolean('status')->default(1)->after('auth_method');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_accounts', function (Blueprint $table) {
            $table->dropColumn('host');
            $table->dropColumn('mail_driver');
            $table->dropColumn('mail_from');
            $table->dropColumn('from_name');
            $table->dropColumn('status');
        });
    }
}
