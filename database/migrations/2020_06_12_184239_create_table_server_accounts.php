<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableServerAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_accounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('server_id')->constrained()->onDelete('cascade');
            $table->string('email', 100);
            $table->string('username', 50);
            $table->string('password', 100);
            $table->text('description');
            $table->integer('port');
            $table->enum('connection_security', ['ssl','tls','none']);
            $table->enum('auth_method', ['no authentication', 'normal password', 'encrypted password', 'kerberros / gssapi', 'ntlm']);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_accounts');
    }
}
