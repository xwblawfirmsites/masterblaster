<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerAccount extends Model
{
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'server_accounts';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id', 'host', 'mail_driver', 'mail_from', 'from_name',
        'email', 'username', 'password', 'description', 'port',
        'connection_security','auth_method'
    ];

     /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

	/**
	 * Relation to server
	 * 
	 * @return Object [description]
	 */
    public function server()
    {
    	return $this->hasOne('App\Server','id','server_id');
    }
}
