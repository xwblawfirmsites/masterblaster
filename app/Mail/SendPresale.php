<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPresale extends Mailable
{
    use Queueable, SerializesModels;

    private $fromEmail = '';
    private $fromName = '';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $fromEmail, $fromName)
    {
        $this->fromEmail = $fromEmail;
        $this->fromName = $fromName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->from($this->fromEmail, $this->fromName)->markdown('emails.presale');
    }
}
