<?php

namespace App\Http\Controllers;

use App\ServerAccount;
use App\Server;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


class ServerAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Server Accounts';
        $mail_driver = ['smtp', 'sendmail', 'mailgun', 'ses', 'postmark', 'localhost','other'];
        $connectionSecurity = ['', 'tls', 'ssl'];
        $authMethod = ['no authentication', 'normal password', 'encrypted password', 'kerberros / gssapi', 'ntlm'];
        return view('server-account',compact('title','mail_driver','connectionSecurity', 'authMethod'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validator = Validator::make($request->all(), [
            'serverName' => 'required',
            'host' => 'required',
            'port' => 'required',
            'serverEmail' => 'required|email',
            'userName' => 'required',
            'password' => 'required',
            'driver' => 'required',
            'driver' => 'required'

        ]);

        if ($validator->fails()) {
            
            return response()->json($validator->messages(),400);
            
        }

        $server = new Server();
        $server->name = $request->serverName;
        $server->save();

        $serverAccount = new ServerAccount();

        $serverAccount->server_id = $server->id;
        $serverAccount->host = $request->host;
        $serverAccount->port = $request->port;
        $serverAccount->email = $request->serverEmail;
        $serverAccount->mail_driver = $request->driver;
        $serverAccount->mail_from = $request->mailFrom;
        $serverAccount->from_name = $request->emailFromName;
        $serverAccount->username = $request->userName;
        $serverAccount->password = $request->password;
        $serverAccount->connection_security = $request->security;
        $serverAccount->auth_method = 'normal password';
        $serverAccount->description = '';
        
        $serverAccount->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServerAccount  $serverAccount
     * @return \Illuminate\Http\Response
     */
    public function show(ServerAccount $serverAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServerAccount  $serverAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(ServerAccount $serverAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServerAccount  $serverAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServerAccount $serverAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServerAccount  $serverAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServerAccount $serverAccount)
    {
        //
    }


    /**
     * Get server name list to datatable
     * 
     * @return [type] [description]
     */
    public function getServers()
    {
        $data['data'] = [];
        $serverAccount = ServerAccount::all();
        if($serverAccount->count()>0){
            foreach ($serverAccount as $key => $value) {
                $data['data'][] = [
                    $value->server->name,
                    $value->mail_driver,
                    $value->host,
                    $value->port,
                    $value->username,
                    ($value->status==0?'<span class="badge badge-secondary">Disabled</span>':'<span class="badge badge-success">Enabled</span>'),
                    ''
                ];
            }
        }

        return response()->json($data);
    }
}
