<?php

namespace App\Http\Controllers;

use App\PreSale;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendPresale;
use Illuminate\Support\Facades\Mail;
//use vendor\swiftmailer\swiftmailer\lib\classes\Swift\Swift_SmtpTransport;
use App\ServerAccount;

class PreSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Presales';
        $emailCount = Person::all()->count();
        return view('presale',compact('title','emailCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Get all presales
     * 
     * @return [type] [description]
     */
    public function getPresales()
    {
        $data['data'] = [];
        $presale = PreSale::all();
        if($presale->count()>0){
            foreach ($presale as $key => $value) {
                $data['data'][] = [
                    $value->name,
                    '',
                    '',
                    '',
                    '<a href="" class="btn btn-sm btn-primary run-presale" data-id="'.$value->id.'"> <i class="fa fa-spinner"> </i> Run</a>'
                ];
            }
        }

        return response()->json($data);
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validator = Validator::make($request->all(), [
            'presaleName' => 'required',
            'presaleFunction' => 'required',
        ]);

        if ($validator->fails()) {
            
            return response()->json($validator->messages(),400);
            
        }

        $preSale = new PreSale;

        $preSale->name = $request->presaleName;
        $preSale->function = $request->presaleFunction;
        $preSale->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreSale  $preSale
     * @return \Illuminate\Http\Response
     */
    public function show(PreSale $preSale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreSale  $preSale
     * @return \Illuminate\Http\Response
     */
    public function edit(PreSale $preSale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreSale  $preSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreSale $preSale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreSale  $preSale
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreSale $preSale)
    {
        //
    }


    public function sendPresale(Request $request)
    {
        

        $email = Person::all()->get($request->emailRow)->email;
        $server = ServerAccount::where('status',1)->inRandomOrder()->first();
        

        $backup = Mail::getSwiftMailer();

        if($server->mail_driver == 'smtp'){
            $transport = new \Swift_SmtpTransport($server->host, $server->port, $server->connection_security);
            $transport->setUsername($server->username)->setPassword($server->password);
        }elseif($server->mail_driver == 'sendmail'){
            $transport = new \Swift_SmtpTransport(config('mail.mailers.sendmail.path'));
        }elseif($server->mail_driver == 'localhost'){
            $transport = new \Swift_SmtpTransport($server->mail_driver,$server->port);
        }

        

        Mail::setSwiftMailer(new \Swift_Mailer($transport));
        

        try {
            
            Mail::to($email)->send(new SendPresale($server->mail_from, $server->from_name));
            $response['status'] = 'success';
            $response['data'] = [
                'sent' => 1,
                'failed' => 0,
                'server' => $server->host,
                'emailRow' => $request->emailRow + 1
            ];
            return response()->json($response);
        } catch (\Exception $e) {

            $response['status'] = 'failed';
            $response['data'] = [
                'message' => $e->getMessage(),
                'sent' => 0,
                'failed' => 1,
                'server' => $server->host,
                'emailRow' => $request->emailRow + 1
            ];
            return response()->json($response,500);
            
        }
        
    }


    public function sendTestEmail()
    {
        // The message
        $message = "Line 1\r\nLine 2\r\nLine 3";

        // In case any of our lines are larger than 70 characters, we should use wordwrap()
        $message = wordwrap($message, 70, "\r\n");

        // Send
        $sent = mail('ssj.simpron.test@gmail.com', 'My Subject', $message);
        dd($sent);
    }
}
