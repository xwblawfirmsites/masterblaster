<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{

	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servers';

     /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


	/**
	 * Relation to Server account
	 * 
	 * @return object [description]
	 */
    public function account()
    {
    	return $this->hasOne('App\ServerAccount','server_id','id');
    }
}
