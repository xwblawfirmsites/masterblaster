<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();


Route::middleware('auth')->group(function () {
    Route::get('/settings', 'SettingsController@index')->name('settings');
    Route::resource('presales', 'PreSaleController');
    Route::resource('server-account', 'ServerAccountController');
    Route::resource('people', 'PersonController');
    Route::get('/get-people', 'PersonController@getPeople');
    Route::get('/get-presales', 'PreSaleController@getPresales');
    Route::post('/send-presale', 'PreSaleController@sendPresale');

    Route::get('/get-servers', 'ServerAccountController@getServers');

    Route::post('/send-test-email', 'PreSaleController@sendTestEmail');

});