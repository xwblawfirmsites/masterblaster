/////////////////////////////////////
///MaterBlaster jQuery/Javascripts //
/////////////////////////////////////
(function($) {
	"use strict";
	var tbl_presale;
	var tbl_server_accounts;
	var tbl_people;
	var emailSent = 0;
	var emailFailed = 0;
    // plugin code here, can use $ because it refers to our argument,
    // not the global symbol that may not be there
    
    /**
     * Sending emails
     * 
     * @param  {[type]} formData  [description]
     * @param  {Number} mailCount [description]
     * @param  {Number} i         [description]
     * @return {[type]}           [description]
     */
    function sendEmails(formData, mailCount = 0, i=1)
    {
    	//let delay = Math.floor(Math.random() * 10000) + 1000;
    	let delay = 4000;
    	
    	if(i<=mailCount){
    		$.ajax({
	            url: xwbVar.sendPresaleURL,
	            type: "post",
	            dataType : 'json',
	            data: formData,
	            success: function(response){
	            	let data = response.data;

	            	formData[0].value = data.emailRow;

	            	emailSent = emailSent+ data.sent;
	            	$("span.email-sent").text(emailSent);

	            	setTimeout(function(){
		    			i++;
		    			sendEmails(formData,mailCount,i);
		    		},delay);
	            	
					
	            },
	            error: function(XMLHttpRequest, textStatus, errorThrown) {
	                let errors = JSON.parse(XMLHttpRequest.responseText);
	                let data = errors.data;
	                
	                emailFailed = emailFailed + data.failed;
	                $("span.email-failed").text(emailFailed);

	                
	                formData[0].value = data.emailRow;
	                setTimeout(function(){
		    			i++;
		    			sendEmails(formData,mailCount,i);
		    		},delay);

	            }
	        });
    	}else{
    		$('.fa-spinner').removeClass('fa-spin');
    	}
    }


    $(function(){
    	/**
    	 * Add presale popup
    	 */
    	$(document).on('click','.add-presale',function(e){
    		e.preventDefault();
    		bootbox.dialog({ 
			    title: 'Add Pre Sale',
			    message: `
			    	<form id="form-add-presale">
			    		<div class="form-group">
						    <label for="presaleName">Name</label>
						    <input type="text" class="form-control" id="presaleName" name="presaleName" aria-describedby="nameHelp" placeholder="Enter Name">
						    <small id="presaleNameHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="presaleFunction">Presale function</label>
						    <select class="form-control" id="presaleFunction" name="presaleFunction" aria-describedby="presaleFunctionHelp" placeholder="Enter Name">
						      	<option>Outbound</option>
						      	<option>Inbound</option>
						      	<option>Event</option>
						      	<option>Other</option>
						    </select>
						    <small id="presaleFunctionHelp" class="form-text text-muted"></small>
						 </div>
			    	</form>
			    `,
			    size: 'large',
			    onEscape: true,
			    backdrop: true,
			    buttons: {
			        save: {
			            label: 'Save',
			            className: 'btn-primary',
			            callback: function(){
			            	let postData = $("#form-add-presale").serializeArray();
			            	$.ajax({
								url: "/presales",
								type: "post",
								data: postData,
								success: function(response){
									tbl_presale.ajax.reload();
									
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									console.log(XMLHttpRequest.responseJSON);
									/*console.log(textStatus);
									console.log(errorThrown);*/
								}
							});
			                                
			            }
			        },
			        close: {
			            label: 'Close',
			            className: 'btn-info',
			            callback: function(){
			                                
			            }
			        }
			    }
			})
    	});

    	/**
    	 * Add server popup
    	 */
    	$(document).on('click','.add-server',function(e){
    		e.preventDefault();
    		let mailDrivers = JSON.parse(xwbVar.mail_driver);
    		let connectionSecurity = JSON.parse(xwbVar.connectionSecurity);
    		let mailDriverOptions = '';
    		let connectionSecurityOptions = '';


    		$.each(mailDrivers, function(i,k){
    			mailDriverOptions += '<option value="'+k+'">'+k+'</option>';
    		});
    		$.each(connectionSecurity, function(i,k){
    			connectionSecurityOptions += '<option value="'+k+'">'+k+'</option>';
    		});

    		bootbox.dialog({ 
			    title: 'Add Server Account',
			    message: `
			    	<form id="form-add-server-account">
			    		<div class="form-group">
						    <label for="serverName">Name</label>
						    <input type="text" class="form-control" id="serverName" name="serverName" aria-describedby="serverNameHelp" placeholder="Enter Name">
						    <small id="serverNameHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="host">Host</label>
						    <input type="text" class="form-control" id="host" name="host" aria-describedby="hostHelp" placeholder="Enter Host">
						    <small id="hostHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="port">Port</label>
						    <input type="text" class="form-control" id="port" name="port" aria-describedby="portHelp" placeholder="Enter port">
						    <small id="portHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="serverEmail">Server Email</label>
						    <input type="email" class="form-control" id="serverEmail" name="serverEmail" aria-describedby="serverEmailHelp" placeholder="Enter server email">
						    <small id="serverEmailHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="userName">User Name</label>
						    <input type="text" class="form-control" id="userName" name="userName" aria-describedby="userNameHelp" placeholder="Enter User Name">
						    <small id="userNameHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="password">Password</label>
						    <input type="password" class="form-control" id="password" name="password" aria-describedby="passwordHelp" placeholder="Enter Password">
						    <small id="passwordHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="driver">Mail Driver</label>
						    <select class="form-control" id="driver" name="driver" aria-describedby="driverHelp" placeholder="Enter mail driver">
						      	`+mailDriverOptions+`
						    </select>
						    <small id="driverHelp" class="form-text text-muted"></small>
						 </div>
						 
						 <div class="form-group">
						    <label for="mailFrom">Mail From</label>
						    <input type="email" class="form-control" id="mailFrom" name="mailFrom" aria-describedby="mailFromHelp" placeholder="Enter mail from">
						    <small id="mailFromHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="emailFromName">Mail From Name</label>
						    <input type="text" class="form-control" id="emailFromName" name="emailFromName" aria-describedby="emailFromNameHelp" placeholder="Enter email from name">
						    <small id="emailFromNameHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="security">Connection Security</label>
						    <select class="form-control" id="security" name="security" aria-describedby="securityHelp" placeholder="Enter connection security">
						      	`+connectionSecurityOptions+`
						    </select>
						    <small id="securityHelp" class="form-text text-muted"></small>
						 </div>
			    	</form>
			    `,
			    size: 'large',
			    onEscape: true,
			    backdrop: true,
			    buttons: {
			        save: {
			            label: 'Save',
			            className: 'btn-primary',
			            callback: function(){
			            	let postData = $("#form-add-server-account").serializeArray();
			            	$.ajax({
								url: "/server-account",
								type: "post",
								data: postData,
								success: function(response){
									tbl_server_accounts.ajax.reload();
									
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									console.log(XMLHttpRequest.responseJSON);
									/*console.log(textStatus);
									console.log(errorThrown);*/
								}
							});
			                                
			            }
			        },
			        close: {
			            label: 'Close',
			            className: 'btn-info',
			            callback: function(){
			                                
			            }
			        }
			    }
			})
    	});


		/**
    	 * Add presale popup
    	 */
    	$(document).on('click','.add-person',function(e){
    		e.preventDefault();
    		bootbox.dialog({ 
			    title: 'Add Person',
			    message: `
			    	<form id="form-add-person">
			    		<div class="form-group">
						    <label for="first_name">First Name</label>
						    <input type="text" class="form-control" id="first_name" name="first_name" aria-describedby="first_nameHelp" placeholder="Enter First Name">
						    <small id="first_nameHelp" class="form-text text-muted"></small>
						</div>
						<div class="form-group">
						    <label for="last_name">Last Name</label>
						    <input type="text" class="form-control" id="last_name" name="last_name" aria-describedby="last_nameHelp" placeholder="Enter First Name">
						    <small id="last_nameHelp" class="form-text text-muted"></small>
						</div>

						<div class="form-group">
						    <label for="email">Email</label>
						    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter Email">
						    <small id="emailHelp" class="form-text text-muted"></small>
						</div>
						
			    	</form>
			    `,
			    size: 'large',
			    onEscape: true,
			    backdrop: true,
			    buttons: {
			        save: {
			            label: 'Save',
			            className: 'btn-primary',
			            callback: function(){
			            	let postData = $("#form-add-person").serializeArray();
			            	$.ajax({
								url: "/people",
								type: "post",
								data: postData,
								success: function(response){
									tbl_presale.ajax.reload();
									
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									console.log(XMLHttpRequest.responseJSON);
									/*console.log(textStatus);
									console.log(errorThrown);*/
								}
							});
			                                
			            }
			        },
			        close: {
			            label: 'Close',
			            className: 'btn-info',
			            callback: function(){
			                                
			            }
			        }
			    }
			})
    	});



    	/**
    	 * presales table records
    	 */
    	tbl_presale = $("#tbl-presale").DataTable({
            //"processing": true, //Feature control the processing indicator.
            //"serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [[ 0, "desc" ]], //Initial no order.
            "ajax": {
                "url": 'get-presales',
                "data": function ( d ) {
                    //d.extra_search = $('#extra').val();
                }
                },
            /*"columnDefs": [
                { 
                    "targets": [ 6 ], 
                    "orderable": false, //set not orderable
                },
            ],*/
            /*"createdRow": function( row, data, dataIndex ) {
                if($(row).find('a.has-action').length != 0){
                    $(row).css('background-color','#dff0d8');
                }
                
            }*/
        });

    	/**
    	 * datatable for server accounts
    	 */
        tbl_server_accounts = $("#tbl-server-account").DataTable({
            "order": [[ 0, "desc" ]], //Initial no order.
            "ajax": {
                "url": 'get-servers',
                "data": function ( d ) {
                    //d.extra_search = $('#extra').val();
                }
            },
        });

        /**
         * Datatable for people
         * 
         */
        tbl_people = $("#tbl-people").DataTable({
            "order": [[ 0, "desc" ]], //Initial no order.
            "ajax": {
                "url": 'get-people',
                "data": function ( d ) {
                    //d.extra_search = $('#extra').val();
                }
            },
        });


        /**
         * Run sending emails
         * 
         */
        $(document).on('click','.run-presale',function(e){
        	e.preventDefault();
        	let id = $(this).data('id');
        	let formData = [];
        	formData.push({
        		'name': 'emailRow',
        		'value': 0,
        	});

        	formData.push({
        		'name': 'presale',
        		'value': id,
        	});

        	$('.fa-spinner').addClass('fa-spin');

        	sendEmails(formData, xwbVar.emailCount);

        });

        $(document).on('click','.xwb-test-email',function(e){
        	e.preventDefault();
        	$.ajax({
				url: "/send-test-email",
				type: "post",
				data: {},
				success: function(response){
					tbl_presale.ajax.reload();
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(XMLHttpRequest.responseJSON);
					/*console.log(textStatus);
					console.log(errorThrown);*/
				}
			});
        });

    	/**
    	 * assigned csrf token to ajax
    	 * 
    	 * @type {Object}
    	 */
    	$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
    });

})(jQuery, document, window);